# diceware.laan.pro

Simple diceware pass-phrase generator. Deployed to [https://diceware.laan.pro](https://diceware.laan.pro).

## References

- [Source](https://gitlab.com/NathanLaan/diceware.laan.pro)
- [EFF Word Lists](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases)
