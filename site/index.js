// dictionary of key-value diceware pairs
let wordlist = {};
let currentDicewarelist = {};

class DicewareList {
  constructor(numberDice, fileName, displayName) {
    this.numberDice = numberDice;
    this.fileName = fileName;
    this.displayName = displayName;
  }
  toString() {
    return this.displayName;
  }
}

const dicewarelistArray = [
  new DicewareList(4, 'eff_short_wordlist_1.txt', 'EFF Short Word List 1'),
  new DicewareList(4, 'eff_short_wordlist_2_0.txt', 'EFF Short Word List 2'),
  new DicewareList(5, 'eff_large_wordlist.txt', 'EFF Large Word List'),
  new DicewareList(5, 'wordlist.txt', 'Original Diceware Word List')
];

function getRandomInt(min, max) {
  const f = crypto.getRandomValues(new Uint32Array(1))[0];
  const i = f / 2 ** 32;
  const r = max - min;
  return Math.floor(i * r + min);
}

function createDiceString(diceCount) {
  let s = '';
  for (i = 0; i < diceCount; i++) {
    s += getRandomInt(1, 6);
  }
  return s;
}

function generatePassPhrase() {
  let wordCount = 6;
  try {
    wordCount = parseInt(document.getElementById('selectWordCount').value, 10);
  } catch (error) {
  }
  let passphrase = '';
  for (let i = 0; i < wordCount; i++) {
    let dice = createDiceString(currentDicewarelist.numberDice);
    let word = wordlist[`'${dice}'`];
    passphrase += word + ' ';
  }
  passphrase.trimEnd();
  document.getElementById('output').innerText = passphrase;
}

function parseWordList(rawString) {
  let lines = rawString.split('\n');
  lines.forEach(function (line) {
    let values = line.split('\t');
    wordlist[`'${values[0]}'`] = values[1];
  });
  console.log(`<<${currentDicewarelist.displayName}>> Diceware List Loaded: ${Object.keys(wordlist).length} words.`);
  generatePassPhrase();
}

function loadWordList() {
  // reset
  wordlist = {};
  let request = new XMLHttpRequest();
  request.open('GET', currentDicewarelist.fileName);
  request.responseType = 'text';
  request.onload = function () {
    parseWordList(request.response);
  };
  request.send();
}

function copyToClipboard() {
  var range = document.createRange();
  range.selectNode(document.getElementById('output'));
  window.getSelection().removeAllRanges();
  window.getSelection().addRange(range);
  document.execCommand('copy');
  window.getSelection().removeAllRanges();
}

function updateCurrentDicewarelist() {
  var element = document.getElementById("selectDicewareList");
  var selectedName = element.value;
  // default!
  currentDicewarelist = dicewarelistArray[0];
  dicewarelistArray.forEach(function(dicewarelistItem) {
    if( selectedName === dicewarelistItem.displayName) {
      currentDicewarelist = dicewarelistItem;
    }
  });
}

function loadDiceWareList() {
  let element = document.getElementById('selectDicewareList');
  dicewarelistArray.forEach(function(dicewarelistItem) {
    let optionElement = document.createElement('option');
    optionElement.textContent = dicewarelistItem.displayName;
    optionElement.value = dicewarelistItem.displayName;
    element.appendChild(optionElement);
  });
  updateCurrentDicewarelist();
}

window.onload = function () {
  loadDiceWareList();
  loadWordList();
  document.getElementById('selectDicewareList').onchange = function () {
    updateCurrentDicewarelist();
    loadWordList();
  }
  document.getElementById('btnGenerate').onclick = function () {
    generatePassPhrase();
  }
  document.getElementById('btnCopy').onclick = function () {
    copyToClipboard();
  }
};